---
title:  "在Linux下使用Inotifity+rsync进行多服务器同步"
date:   2015-05-20 21:0:00
categories: Inotifity
excerpt: 本文主要记录了多台PHP服务器之间的代码同步问题，使用Linux的Inotify机制实现代码多态服务之间的严格同步
---
# 在Linux下使用Inotifity+rsync进行多服务器同步
&#8195;游戏马上就要上线了，由于联运方的估算，开服的初期，可能会有大量的玩家进入，那么服务器的负载就是一个急需解决的问题。我们的游戏是基于HTTP的架构，根据以往的几个项目的做法，在前端打包的时候，根据不同的平台和运营平台，链接不同的服务器，也就是每个前端打包设定不同的url，这样子的坏处极其明显，繁重且容易出错，而且无法充分的利用到服务器资源。因此，在百般查找和思索过后，决定利用Nginx的负载均衡机制进行多服务器的负载均衡。

&#8195;Nginx和PHP-FPM的多服务器负载均衡配置网上有很多现成的方案，再次不做累述，但是多服务器的流量负载，各个服务器上跑的代码就必须保证一致，由于我在项目上是利用SVN的post-commit机制来进行代码的版本库管理和自动提交到生产和测试服务器，如果把所有的服务器的都写进post-commit中，那么每次commit，可能会由于服务器过多，导致rsync运行过长，每次等待也是一种煎熬。因此就利用起来linux自2.6.13内核以来，新的Inotifity机制在多服务器之间进行文件同步。

&#8195;Linux以往使用使用shell脚本配合crontab监控文件的改动，这是在内核2.6.13之前迫不得已的做法，还有一种就是使用dnotify

> 和 dnotify 相比 inotify 的优势如下：
&#8195;&#8195;Inotify 使用一个独立的文件描述符，而 dnotify 需要为每个受监控的目录打开一个文件描述符。当您同时监控多个目录时成本会非常高，而且还会遇到每进程文件描述符限制。
&#8195;&#8195;Inotify 所使用的文件描述符可以通过系统调用获得，并且没有相关设备或者文件。而使用 dnotify，文件描述符就固定了目录，妨碍备用设备卸载，这是可移动媒体的一个典型问题。对于 inotify，卸载的文件系统上的监视文件或目录会产生一个事件，而且监视也会自动移除。
&#8195;&#8195;Inotify 能够监视文件或者目录。Dnotify 则只监视目录，因此程序员还必须保持 stat 结构或者一个等效的数据结构，来反映该被监视目录中的文件，然后在一个事件发生时，将其与当前状态进行对比，以此了解当前目录中的条目发生了什么情况。
&#8195;&#8195;如上所述，inotify 使用文件描述符，允许程序员使用标准 select 或者 poll 函数来监视事件。这允许高效的多路复用 I/O 或者与 Glib 的 mainloop 的集成。相比之下，dnotify 使用信号，这使得程序员觉得比较困难或者不够流畅。在 2.6.25 内核中 inotify 还添加了 Signal-drive I.O 通知功能。

&#8195;inotify只是linux内核的一个feature，为此，我们需要安装inotify-tools这个工具来使用这个feature，首先，我们检查一下能否使用inotify

{% highlight Bash %}

[root@rsync-client ~]# ll /proc/sys/fs/inotify/*
-rw-r--r-- 1 root root 0 Jun  4 15:30 /proc/sys/fs/inotify/max_queued_events
-rw-r--r-- 1 root root 0 Jun  4 15:30 /proc/sys/fs/inotify/max_user_instances
-rw-r--r-- 1 root root 0 Jun  4 15:30 /proc/sys/fs/inotify/max_user_watches

{% endhighlight %}

以上结果表明内核支持inotify，inotify提供两个工具inotifywait和inotifywatch
> &#8195;&#8195;inotifywait 仅执行阻塞，等待 inotify 事件，你可以使用它来监控任何一组文件和目录，或监控整个目录树（目录、子目录、子目录的子目录等等），并且可以结合 shell 脚本，更好的使用 inotifywait。
&#8195;&#8195;inotifywatch 用来收集关于被监视的文件系统的统计数据，包括每个 inotify 事件发生多少次。

inotifywait

> inotifywait [-hcmrq] [-e ] [-t ] [--format ] [--timefmt ] [ ... ]

具体的参数解释参考如下

> -h,–help    # 输出帮助信息
@     # 排除不需要监视的文件，可以是相对路径，也可以是绝对路径
–fromfile   # 从文件读取需要监视的文件或排除的文件，一个文件一行，排除的文件以@开头
-m,–monitor # 接收到一个事情而不退出，无限期地执行。默认行为是接收到一个事情后立即退出
-d,–daemon  # 跟–monitor一样，除了是在后台运行，需要指定 –outfile把事情输出到一个文件。也意味着使用了–syslog
-o,–outfile # 输出事情到一个文件而不是标准输出。
-s,–syslog  # 输出错误信息到系统日志
-r,–recursive # 监视一个目录下的所有子目录。
-q,–quiet   # 指定一次，不会输出详细信息，指定二次，除了致命错误，不会输出任何信息。
–exclude    # 正则匹配需要排除的文件，大小写敏感。
–excludei   # 正则匹配需要排除的文件，忽略大小写。
-t，–timeout# 设置超时时间，如果为0，则无限期地执行下去。
-e,–event   # 指定监视的事件。
-c,–csv     # 输出csv格式。
–timefmt    # 指定时间格式，用于–format选项中的%T格式。
–format     # 指定输出格式。
   %w 表示发生事件的目录
   %f 表示发生事件的文件
   %e 表示发生的事件
   %Xe 事件以“X”分隔
   %T 使用由–timefmt定义的时间格式
   
   inotifywatch
> inotifywatch [-hvzrqf] [-e ] [-t ] [-a ] [-d ] [ ... ]

参数解释如下

> -h，–help    # 输出帮助信息
-v，–verbose # 输出详细信息
@             # 排除不需要监视的文件，可以是相对路径，也可以是绝对路径。
–fromfile    # 从文件读取需要监视的文件或排除的文件，一个文件一行，排除的文件以@开头。
-z，–zero    # 输出表格的行和列，即使元素为空
–exclude     # 正则匹配需要排除的文件，大小写敏感。
–excludei    # 正则匹配需要排除的文件，忽略大小写。
-r，–recursive  # 监视一个目录下的所有子目录。
-t，–timeout    # 设置超时时间
-e，–event      # 只监听指定的事件。
-a，–ascending  # 以指定事件升序排列。
-d，–descending # 以指定事件降序排列

OK，如上所述，由于项目使用了多台PHP服务器进行业务逻辑代码处理，所以我们就需要在其中的一台服务器上监控项目文件夹，我这里选择使用SVN的POST-COMMIT的服务器作为监控服务器，使用如下脚本监控

{% highlight Bash %}

#!/bin/bash
# 利用inotify所要监控的目录
src=/data/www/html/mu_project/
#利用rsync同步目录
inotifywait -mrq --timefmt '%d/%m/%y %H:%M' --format '%T %w %w%f %e' -e modify,delete,create,attrib,move $src | while read DATE TIME DIR FILE EVENT;
do
# /usr/local/rsync/bin/rsync -vzrtopg --delete --progress --password-file=/usr/local/rsync/rsync.passwd $src $user@$host::$des >>dev/null 2>>/tmp/rsync.log
/bin/bash /data/www/html/mu_project/mu_shell/rsync/syncStrict.sh
done

{% endhighlight %}

每次该服务器上的mu_project下的文件有过修改，删除创建等需要同步的操作，，脚本会自动触发rsync命令对远程服务器进行文件同步，这样，多服务器之间的代码严格一致性就得到解决了。相对于手动运行同步脚本或者SVN的post-commit的复杂性，inotify真正做到了一次修改，处处同步，解放了每次同步的繁杂操作，真正的使得项目的程序员更加的关注业务代码的逻辑编写。
