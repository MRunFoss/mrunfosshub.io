---
title:  在线简历
date:   2015-06-24 21:0:00
categories: Job Resume
excerpt: 在线简历
---
# 个人信息

---
 - 吴俊驰/男/1990年08月
 - 本科/桂林理工大学通信工程（2009/09~2013/06）
 - 工作年限：2年
 - 技术博客：https://mrunfoss.github.io
 - GItHub：https://github.com/MRunFoss
 - 本简历在线MarkDown版本:https://mrunfoss.github.io/job/resume/2015/06/24/jobresume.html 
 - 联系方式：

> 手机：13122732916  邮箱：mrun.foss@foxmail.com

 - 期望职位:PHP后端程序员
 - 目前地点：深圳

---
 
#工作经历

##上海创动信息科技有限公司（2013年7月~2015年6月）手游后端服务器开发

> 主要职责是基于LNRP(Linux+Nginx+Redis+PHP),开发手游的后端服务器，日常的维护以及管理。作为后端主力程序员，与公司其他同事一起重新编写了一套服务器开发框架，用于公司大部分项目的开发。

###[奇迹挂机手游项目](http://qjgj.gz.1251001087.clb.myqcloud.com/mu_backup_tx/html5/)
    

> 后端开发负责人，在项目中负责新的框架开发以及维护管理，在该项目中，使用了nginx+varnish和nginx的多负载均衡，和全新的后端框架,使得预计的服务器数量缩减，同时没有影响到线上的用户体验，节约了公司的服务器成本，全新的框架也尽可能的改善了前期项目所遇见的问题。最后，成功在腾讯玩吧平台上线。

###[火影总动员](http://www.ptbus.com/rzzdy/)

> 后端开发负责人，在项目中负责后端框架的建设与维护，以及后端的代码编写，带领多名PHP程序员保证了该游戏稳定的上线运营。在该项目中，重构了大量过去老框架使用的成就代码，尽量使用更加优秀的代码设计以及逻辑模块

###[女帝](http://ku.18183.com/nvdi.html)

> 后端开发程序员，负责业务的逻辑编写和服务器管理维护。在该项目中，最主要的是身为应届毕业生，实现了一套运营日志分析和CMS运营管理系统，使得游戏的市场数据分析更加的直观，更为方便的管理了玩家的游戏数据。

##技术文章

 - [在LINUX下使用INOTIFITY+RSYNC进行多服务器同步](https://mrunfoss.github.io/inotifity/2015/05/20/inotify_rsync.html)

##技能清单

 - 掌握PHP，能使用python/lua/nodejs/shell，熟悉js/html等常用技术
 - 能使用linux开发，项目多使用LNRP(Linux+Nginx+Redis+PHP)开发
 - 数据库相关：mysql/redis
 - 版本管理、文档和自动化部署工具:SVN/Git/PHPDoc
 - Debug性能分析工具：xhprof
 - 云和开放平台:各大国内主流的开发平台接入，Ucloud、金山云等
 - 开发工具:Vim/Sublime等

 
    




